<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::any('wechat',   'WechatController@serve');
Route::any('create-menu',   'WechatController@createMenu');


Route::get('test','IndexController@test');


Route::get('login',     'IndexController@login');
Route::any('logindo',   'IndexController@loginDo');
Route::middleware(['wechat.oauth'])->group(function(){
	
	Route::middleware(['checkUser'])->group(function(){ 	
	 	Route::get('/',         'IndexController@index');
	 	Route::get('query-flow','IndexController@queryFlow');
	 	Route::get('offer-dtl', 'IndexController@offerDtl');
	 	Route::any('recharge',  'IndexController@recharge');
	 	Route::get('log','IndexController@log');
	});
});



//Route::middleware(['web','wechat.oauth'])->prefix('user')->group(function(){	
	
//});

Route::prefix('zadmin')->namespace('Admin')->group(function(){
	Route::middleware('checkAdmin')->group(function(){
		
		
		Route::resource('cards',      'CardController');   // 物联卡
		Route::resource('users',      'UserController');  
		Route::resource('dataplans',  'DataplanController'); //套餐
		Route::resource('orders',     'OrderController');
		
		//后台首页
		Route::get('/',		   'IndexController@index');
		
	});
		Route::get('login',    'LoginController@login');
		Route::post('logindo', 'LoginController@loginDo');
		Route::get('logout',   'LoginController@logOut');
		
	
});






