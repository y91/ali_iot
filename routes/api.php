<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

	Route::any('make-order','IndexController@makeOrder');
	Route::any('pay-notify','IndexController@payNotify');

	// 扫码支付
	Route::any('scheme-code','IndexController@schemeCode');

