<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $guarded = ['id'];

    public function dataplan()
    {
    	return $this->belongsTo(Dataplan::class);
    }

    public function getPriceTxtAttribute()
    {
    	return $this->price/100;
    }

}
