<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use EasyWeChat\Kernel\Messages\Image;
use EasyWeChat\Kernel\Messages\Media;
use EasyWeChat\Kernel\Messages\News;
use EasyWeChat\Kernel\Messages\NewsItem;
use EasyWeChat\Kernel\Messages\Article;
use Log;

class WechatController extends Controller
{
    /**
     * 处理微信的请求消息
     *
     * @return string
     */
    public function serve()
    {
        Log::info('request arrived.'); # 注意：Log 为 Laravel 组件，所以它记的日志去 Laravel 日志看，而不是 EasyWeChat 日志

        $app = app('wechat.official_account');
        //dd($app);
        $app->server->push(function($message){
            // 图片素材
            //w9bomHYP49GlIbdEEbg088DdU4L2mpHtgdO7uUvB8-4
            //return new Image('w9bomHYP49GlIbdEEbg088DdU4L2mpHtgdO7uUvB8-4');

            // 图文素材
            //w9bomHYP49GlIbdEEbg088C_VflOMpWhFvTetVvKY24
            return "欢迎关注!业务问题请加微信 bzs1984 咨询，谢谢。";

           
         
            
        });

        return $app->server->serve();
    }

    public function createMenu()
    {
        $app = app('wechat.official_account');
        $buttons = [
           
                    [
                        "type" => "view",
                        "name" => "提交工单",
                        "url"  => "http://aomen.qdss.com.cn/user/task/create"
                    ],
                    [
                        "type" => "view",
                        "name" => "我的工单",
                        "url"  => "http://aomen.qdss.com.cn/user/task/"
                    ],
                    [
                        "name"       => "服務信息",
                        "sub_button" => [
                            [
                                "type" => "view",
                                "name" => "公告",
                                "url"  => "http://www.soso.com/"
                            ],
                            [
                                "type" => "view",
                                "name" => "服務日誌",
                                "url"  => "http://v.qq.com/"
                            ],
                            [
                                "type" => "view",
                                "name" => "人力支援",
                                "key" => "V1001_GOOD"
                            ],
                        ],
                    ],    
                   
            
           
        ];
        $rs = $app->menu->create($buttons);
        if($rs['errcode']==0) {
            return response()->json(['status'=>'success','msg'=>'操作成功']);
        } 
        return response()->json(['status'=>'danger','msg'=>$rs['errmsg']]);
    }

    public function getMaterial($type='image')
    {
        $app = app('wechat.official_account');
        // 图片素材
        $list = $app->material->list('image', 0, 10);

        // 图文素材
        //$list = $app->material->list('news', 0, 10);

        return view('admin.material.index');
    }

   


    
}
