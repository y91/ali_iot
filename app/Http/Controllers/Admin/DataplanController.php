<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Dataplan;
use Aliyun\DySDKLite\Iot;

class DataplanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $offers = [];
        $rs = Iot::queryIotCardOfferDtl('8986031848206552925');
        if($rs['status'] === true) {
            $offers = $rs['data'];
        }
        //dd($offers);
        $list = Dataplan::paginate(20);
        return view('admin.dataplan.index',compact('list','offers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rs['status'] = 'danger';
        $rs['msg'] = '操作失败';
        $data = $request->except('_token');
        $flag = Dataplan::create($data);
        if($flag) {
            $rs['status'] = 'success';
            $rs['msg'] = '操作成功';
            return back()->with('rs',$rs);
        }

        return back()->with('rs',$rs);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Dataplan::find($id);  
        $offers = [];
        $rs = Iot::queryIotCardOfferDtl('8986031848206552925');
        if($rs['status'] === true) {
            $offers = $rs['data'];
        }    
        return view('admin.dataplan.edit',compact('data','offers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rs['status'] = 'danger';
        $rs['msg']    = '操作失败';
        $data = $request->except('_token','_method');
        $flag = Dataplan::where('id',$id)->update($data);
        if($flag) {
           $rs['msg'] = 'success';
           $rs['msg'] = '操作成功';
           return redirect('zadmin/cards')->with('rs',$rs);
        }
        
        return back()->with('rs',$rs);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rs['status'] = 'danger';
        $rs['msg']    = '操作失败';
        $flag         = Dataplan::destroy($id);
        if ($flag) {
            $rs['status'] = 'success';
            $rs['msg']    = '删除成功';
            return back()->with('rs',$rs); 
        }
        return back()->with('rs',$rs);
    }
}
