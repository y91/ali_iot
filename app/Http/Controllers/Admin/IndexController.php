<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Task;

use App\Repos\TaskRepo;
use Log,Session;
use App\Services\Wechat;

class IndexController extends Controller
{
    public function index()
    {
    	$list =Order::take(10)->orderBy('created_at','desc')->get();    	
    	return view('admin.index',compact('list'));
    }

    
}
