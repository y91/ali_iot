<?php
namespace Aliyun\DySDKLite;
use Aliyun\DySDKLite\SignatureHelper;
use Log;


class Iot 
{
    private static  $security        = false;    
    private static  $accessKeyId     = "LTAIiyJQ7ubOjdcs"; 
    private static  $accessKeySecret = "Y0wm3QZc5XBzoyJ7aTAaB2OPhDkx9w";  

    /**
     * 物联网卡流量查询
     */
    public static function  queryCardFlowInfo($iccid)
    {

        $params = [];

        // fixme 必填: 待查询的物联卡对应的Iccid编码
        //$params["Iccid"] = "8986031848206552925";
        $params["Iccid"] = $iccid;
       

        // 初始化SignatureHelper实例用于设置参数，签名以及发送请求
        $helper = new SignatureHelper();

        // 此处可能会抛出异常，注意catch
        $content = $helper->request(
            self::$accessKeyId,
            self::$accessKeySecret,
            "dyiotapi.aliyuncs.com",
            array_merge($params, array(
                "RegionId" => "cn-hangzhou",
                "Action"   => "QueryCardFlowInfo",
                "Version"  => "2017-11-11",
            )),
            self::$security
        );

        return $content;
    }


    /**
     * 物联卡当前时间有效套餐列表查询
     */
    public static function queryIotCardOfferDtl($iccid)
    {
        $rs['status'] = false;
        $params = [];         

        // fixme 必填: 待查询的物联卡对应的Iccid编码
        //$params["Iccid"] = "8986031848206552925";
        $params['Iccid'] = $iccid;

      

        // 初始化SignatureHelper实例用于设置参数，签名以及发送请求
        $helper = new SignatureHelper();

        // 此处可能会抛出异常，注意catch
        try {
            $content = $helper->request(
                self::$accessKeyId,
                self::$accessKeySecret,
                "dyiotapi.aliyuncs.com",
                array_merge($params, array(
                    "RegionId" => "cn-hangzhou",
                    "Action"   => "QueryIotCardOfferDtl",
                    "Version"  => "2017-11-11",
                )),
                self::$security
            );
        } catch (\Exception $e) {
            $rs['msg'] = $e->getMessage();
        }
        
        $rs['status'] = true;
        //d($content);
        $rs['data'] = array_pluck($content->CardOfferDetail->detail,'OfferName', 'OfferId');;
        return $rs;
    }


    /**
     * 物联网卡流量充值
     * @param iccid 待查询的物联卡对应的Iccid编码，
     * @param offerid 充值的套餐编号，
     * @param effcode：套餐生效类型：AUTO_ORD(续订) 或者1000(立即生效)
     */
    public static function doIotRecharge($iccid,$offerid,$effcode='1000')
    {

        $params = array ();    

        // fixme 必填: 
        $params["Iccid"]    = $iccid;

      
        //$params["OfferIds"] = "22010000250054";
        $params['OfferIds'] = $offerid;
        $params["EffCode"]  = $effcode;

        //$params["OutId"] = "yourOutId";
        $params["OutId"] = $iccid.'-'.date('YmdHis').'-'.$effcode;

        // fixme 必须: 自定义流量套餐流量数，单位KB；该参数配合自定义档位套餐使用，目前未开放填 0
        $params["Amount"] = 0;

        // *** 需用户填写部分结束, 以下代码若无必要无需更改 ***

        // 初始化SignatureHelper实例用于设置参数，签名以及发送请求
        $helper = new SignatureHelper();

        try {
            // 此处可能会抛出异常，注意catch
            $content = $helper->request(
                self::$accessKeyId,
                self::$accessKeySecret,
                "dyiotapi.aliyuncs.com",
                array_merge($params, array(
                    "RegionId" => "cn-hangzhou",
                    "Action" => "DoIotRecharge",
                    "Version" => "2017-11-11",
                )),
                self::$security
            );
        } catch (\Exception $e) {
            Log::warning('充值失败：'.$e->getMessage());
        }

        return $content;
    }

    /**
     * 物联网卡状态信息
     *
     * @return stdClass
     * @throws ClientException
     */
    public static function queryCardStatus($iccid)
    {
        $params = array ();

        // *** 需用户填写部分 ***
        // fixme 必填：是否启用https
        $security = false;


        // fixme 必填: 待查询的iccid值
        $params["Iccid"] = $iccid;

        // *** 需用户填写部分结束, 以下代码若无必要无需更改 ***

        // 初始化SignatureHelper实例用于设置参数，签名以及发送请求
        $helper = new SignatureHelper();

        // 此处可能会抛出异常，注意catch
        $content = $helper->request(
            self::$accessKeyId,
            self::$accessKeySecret,
            "dyiotapi.aliyuncs.com",
            array_merge($params, array(
                "RegionId" => "cn-hangzhou",
                "Action" => "QueryCardStatus",
                "Version" => "2017-11-11",
            )),
            $security
        );

        return (array)$content;
    }




}