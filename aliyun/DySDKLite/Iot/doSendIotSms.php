<?php
/*
 * 此文件用于验证物联网卡无线连接服务API接口，供开发时参考
 * 执行验证前请确保文件为utf-8编码，并替换相应参数为您自己的信息，并取消相关调用的注释
 * 建议验证前先执行Test.php验证PHP环境
 *
 * 2017/11/30
 */

namespace Aliyun\DySDKLite\Iot\Demo;

require_once dirname(__DIR__) . "/SignatureHelper.php";

use Aliyun\DySDKLite\SignatureHelper;

// todo 接口定义，请先替换相应参数为您自己的信息

/**
 * 物联卡短信发送
 */
function doSendIotSms() {

    $params = array ();

    // *** 需用户填写部分 ***
    // fixme 必填：是否启用https
    $security = false;

    // fixme 必填: 请参阅 https://ak-console.aliyun.com/ 取得您的AK信息
    $accessKeyId = "your access key id";
    $accessKeySecret = "your access key secret";

    // fixme 必填: 物联卡短信签名
    $params["SignName"] = "yourIotSmsSignName";

    // fixme 必填: 物联卡短信模板code  注：物联卡数据短信模板code 和 物联卡普通短信code 是分开的
    $params["TemplateCode"] = "yourIotSmsCode";

    // fixme 必填: 接受短信的物联卡MSISDN
    $params["PhoneNumbers"] = "receiverNumber";

    // fixme 必填: 设置上下文参数 注：无论物联卡普通短信还是物联卡数字短信上下文参数key均为content
    $params["TemplateParam"] = array('content' => 'abc');


    // *** 需用户填写部分结束, 以下代码若无必要无需更改 ***
    if(!empty($params["TemplateParam"]) && is_array($params["TemplateParam"])) {
        $params["TemplateParam"] = json_encode($params["TemplateParam"], JSON_UNESCAPED_UNICODE);
    }

    // 初始化SignatureHelper实例用于设置参数，签名以及发送请求
    $helper = new SignatureHelper();

    // 此处可能会抛出异常，注意catch
    $content = $helper->request(
        $accessKeyId,
        $accessKeySecret,
        "dyiotapi.aliyuncs.com",
        array_merge($params, array(
            "RegionId" => "cn-hangzhou",
            "Action" => "DoSendIotSms",
            "Version" => "2017-11-11",
        )),
        $security
    );

    return $content;
}

ini_set("display_errors", "on"); // 显示错误提示，仅用于测试时排查问题
// error_reporting(E_ALL); // 显示所有错误提示，仅用于测试时排查问题
set_time_limit(0); // 防止脚本超时，仅用于测试使用，生产环境请按实际情况设置
header("Content-Type: text/plain; charset=utf-8"); // 输出为utf-8的文本格式，仅用于测试

// 验证物联卡短信发送(DoSendIotSms)接口
print_r(doSendIotSms());