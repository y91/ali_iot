<?php
/*
 * 此文件用于验证物联网卡无线连接服务API接口，供开发时参考
 * 执行验证前请确保文件为utf-8编码，并替换相应参数为您自己的信息，并取消相关调用的注释
 * 建议验证前先执行Test.php验证PHP环境
 *
 * 2017/11/30
 */

namespace Aliyun\DySDKLite\Iot\Demo;

require_once dirname(__DIR__) . "/SignatureHelper.php";

use Aliyun\DySDKLite\SignatureHelper;


// todo 接口定义，请先替换相应参数为您自己的信息

/**
 * 物联网卡流量查询
 */
function queryCardFlowInfo() {

    $params = array ();

    // *** 需用户填写部分 ***
    // fixme 必填：是否启用https
    $security = false;

    // fixme 必填: 请参阅 https://ak-console.aliyun.com/ 取得您的AK信息
    $accessKeyId     = "LTAIiyJQ7ubOjdcs";
    $accessKeySecret = "Y0wm3QZc5XBzoyJ7aTAaB2OPhDkx9w";

    // fixme 必填: 待查询的物联卡对应的Iccid编码
    $params["Iccid"] = "8986031848206552925";

    // *** 需用户填写部分结束, 以下代码若无必要无需更改 ***

    // 初始化SignatureHelper实例用于设置参数，签名以及发送请求
    $helper = new SignatureHelper();

    // 此处可能会抛出异常，注意catch
    $content = $helper->request(
        $accessKeyId,
        $accessKeySecret,
        "dyiotapi.aliyuncs.com",
        array_merge($params, array(
            "RegionId" => "cn-hangzhou",
            "Action" => "QueryCardFlowInfo",
            "Version" => "2017-11-11",
        )),
        $security
    );

    return $content;
}

ini_set("display_errors", "on"); // 显示错误提示，仅用于测试时排查问题
// error_reporting(E_ALL); // 显示所有错误提示，仅用于测试时排查问题
set_time_limit(0); // 防止脚本超时，仅用于测试使用，生产环境请按实际情况设置
header("Content-Type: text/plain; charset=utf-8"); // 输出为utf-8的文本格式，仅用于测试

// 验证物联网卡流量查询(QueryCardFlowInfo)接口
print_r(queryCardFlowInfo());