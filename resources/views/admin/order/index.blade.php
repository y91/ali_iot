@extends('admin.layouts.app')
@section('content')

                        <!-- Page-Title -->
                        <div class="row">
                            <div class="col-sm-12">
                                

                                <h4 class="page-title">控制台</h4>
                                <p class="text-muted page-title-alt"></p>
                            </div>
                        </div>

						

                        


                        <div class="row">

                            <div class="col-lg-12">

                                <div class="portlet"><!-- /primary heading -->
                                    <div class="portlet-heading">
                                        <h3 class="portlet-title text-dark text-uppercase">
                                            充值订单
                                        </h3>
                                        
                                        <div class="clearfix"></div>
                                    </div>
                                    <div id="portlet2" class="panel-collapse collapse in">
                                        <div class="portlet-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover ">
                                            <thead>
                                                
                                                    
                                                    <th>流量卡号</th>
                          
                          
                          
                                                    
                         <th>套餐</th>
                         <th>金额</th>
                          <th>时间</th>
                                                </tr>
                                            </thead>
                                            
                                            <tbody>

                                            @foreach($list as $v)
                                               <tr>
                                                   
                                                    
                                                 
                                                    
                                                    
                                                     <td>
                                                        {{$v->iccid}}
                                                    </td>
                                                   

                                                    <td>
                                                        {{$v->dataplan->name or ''}}
                                                    </td>
                                                
                                                   
                                                           <td>
                                                        {{$v->price}}
                                                    </td>
                                                    <td>
                                                        {{$v->created_at}}
                                                    </td>
                                                   
                                                </tr>
                                            @endforeach    
                                                
                                               
                                               
                                                
                                               
                                                
                                            
                                            </tbody>
                                        </table>

                                            </div>
                                       {{$list->links()}}
                                        </div>
                                    </div>
                                </div>
                            </div> <!-- end col -->


                        </div>

						<!-- end row -->

                        
@endsection 

 @section('modal')        
        <!-- Modal -->
      <div class="modal fade" id="refundModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
          &times;
        </button>
        <h4 class="modal-title" id="myModalLabel">
          退单
        </h4>
      </div>
       <form class="form-horizontal" role="form" action="{{url('zadmin/refund')}}" method="post">
      <div class="modal-body">
       
         <div class="form-group">
                  <label class="control-label">退单原因</label>
                 
                    <textarea class="form-control"  name="refund_reason" id="refund_reason"></textarea>
                 
                </div> 
     
       <input type="hidden" name="task_id" value="" id="task_id" />
       {{csrf_field()}}
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭
        </button>
        <button type="submit" class="btn btn-primary">
          提交
        </button>
      </div>
    </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal -->
</div> 
    @endsection 


@section('js')
<!-- Counterup  -->
        
        <script src="{{asset('admin/plugins/counterup/jquery.counterup.min.js')}}"></script>

        <script src="{{asset('admin/plugins/morris/morris.min.js')}}"></script>
        <script src="{{asset('admin/plugins/raphael/raphael-min.js')}}"></script>
        
        <script type="text/javascript">
            $('.btn-refund').on('click',function(){
                var task_id = $(this).data('id');
                $("#refundModal").modal('show');
                $('#task_id').val(task_id);
            })
        </script>
      
@endsection