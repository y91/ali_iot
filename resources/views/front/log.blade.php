<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width,height=device-height,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!--1.width=device-width    //应用程序的宽度和屏幕的宽度是一样的
2.height=device-height  //应用程序的高度和屏幕的高是一样的
3.initial-scale=1.0  //应用程序启动时候的缩放尺度（1.0表示不缩放）
4.minimum-scale=1.0  //用户可以缩放到的最小尺度（1.0表示不缩放）
5.maximum-scale=1.0  //用户可以放大到的最大尺度（1.0表示不缩放）
6.user-scalable=no  //用户是否可以通过他的手势来缩放整个应用程序，使应用程序的尺度发生一个改变（yes/no）
    -->
    <!-- 优先使用 IE 最新版本和 Chrome -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <!-- 启用360浏览器的极速模式(webkit) -->
    <meta name="renderer" content="webkit">
    <link rel="stylesheet" href="/front/bootstrap-3.3.6-dist/css/bootstrap.min.css">

    <title>个人中心</title>
    <style>
        *{
            marign:0;
            padding:0
        }
        li{
            list-style: none;
        }
        body {
            background: #f0f1f3;
            font-size: 14px;
        }
        .text-orange{
            color:#FF7F42;
        }
        .head-box {
            padding: 20px 15px;
            background: #0d73bd url("bg.png");
            background-size: cover;
        }

        .head-img-box {
            margin-right: 2%;
            float: left;
        }

        .head-img {
            width: 80px;
            height: 80px;
        }

        .member-name-box {
            width: 70%;
            margin: 12px 0 0 0;
            float: left;
            font-size: 14px;
            color: #fff;
        }
        .account-box{
            width:100%;
            height:46px;
            line-height: 46px;
            display:flex;
        }
        .account-box li{
            float:left;
            flex:1;
            background: #fff;
            text-align: center;
        }
        .account-box li:first-child{
            margin-right:2px;
        }
        .menu-box{
            margin-top:15px;
        }
        .menu-box li{
            width:33%;
            margin:0 1px 1px 0;
            padding:20px 0;
            float:left;
            background: #fff;
            font-size:16px;
            color:#333;
            text-align: center;
        }
        .menu-box li:last-child(3),
        .menu-box li:last-child{
            margin-right:0;
        }
        .menu-box li .menu-icon{
            display: block;
            width:38px;
            height:38px;
            margin:0 auto 5px auto;
        }
    </style>
</head>
<body>
<div class="head-box clearfix">
    <div class="head-img-box"><img src="{{session('user')->avatar}}" alt="" class="img-circle head-img"></div>
    <div class="member-name-box">
        <p>昵称：{{session('user')->nickname}}</p>
        <p style="word-break:break-all;">卡号：{{session('user')->iccid}}</p>
    </div>
</div>
<!-- <ul class="account-box clearfix">
    <li>账户余额：<span class="text-primary">0</span> 元</li>
    <li>认证状态：<span class="text-orange">未认证</span></li>
</ul> -->
@if($list)
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">充值记录</div>

  <!-- Table -->
  <table class="table">
    <tr>
        <td>时间</td>
        <td>金额</td>
        <td>事项</td>
    </tr>
    @foreach($list as $v)
    <tr>
        <td>{{$v->created_at}}</td>
        <td>{{$v->price_txt}}</td>
        <td>{{$v->msg}}</td>
    </tr>
    @endforeach
   
    
  </table>
  {{$list->links()}}
  <div style="float: right;margin-right:10px; ">
    <a href="{{url('/')}}" class="btn btn-primary navbar-btn">返回</a>
  </div>
</div>
@else
<div class="alert alert-info" role="alert">没有查询到充值记录</div>
@endif
<script src="/front/bootstrap-3.3.6-dist/js/jquery.min.js"></script>
<script src="/front/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
</body>
</html>