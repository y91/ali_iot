<div class="head-box clearfix">
    <div class="head-img-box"><img src="{{session('user')->avatar}}" alt="" class="img-circle head-img"></div>
    <div class="member-name-box">
        <p>昵称：{{session('user')->nickname}}</p>
        <p style="word-break:break-all;">卡号：{{session('user')->iccid}}</p>
    </div>
</div>