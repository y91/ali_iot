<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width,height=device-height,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!--1.width=device-width    //应用程序的宽度和屏幕的宽度是一样的
2.height=device-height  //应用程序的高度和屏幕的高是一样的
3.initial-scale=1.0  //应用程序启动时候的缩放尺度（1.0表示不缩放）
4.minimum-scale=1.0  //用户可以缩放到的最小尺度（1.0表示不缩放）
5.maximum-scale=1.0  //用户可以放大到的最大尺度（1.0表示不缩放）
6.user-scalable=no  //用户是否可以通过他的手势来缩放整个应用程序，使应用程序的尺度发生一个改变（yes/no）
    -->
    <!-- 优先使用 IE 最新版本和 Chrome -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <!-- 启用360浏览器的极速模式(webkit) -->
    <meta name="renderer" content="webkit">
    <link rel="stylesheet" href="/front/bootstrap-3.3.6-dist/css/bootstrap.min.css">

    <title>个人中心</title>
    <style>
        *{
            marign:0;
            padding:0
        }
        li{
            list-style: none;
        }
        body {
            background: #f0f1f3;
            font-size: 14px;
        }
        .text-orange{
            color:#FF7F42;
        }
        .head-box {
            padding: 20px 15px;
            background: #0d73bd url("/front/bg.png");
            background-size: cover;
        }

        .head-img-box {
            margin-right: 2%;
            float: left;
        }

        .head-img {
            width: 80px;
            height: 80px;
        }

        .member-name-box {
            width: 70%;
            margin: 12px 0 0 0;
            float: left;
            font-size: 14px;
            color: #fff;
        }
        .account-box{
            width:100%;
            height:46px;
            line-height: 46px;
            display:flex;
        }
        .account-box li{
            float:left;
            flex:1;
            background: #fff;
            text-align: center;
        }
        .account-box li:first-child{
            margin-right:2px;
        }
        .menu-box{
            margin-top:15px;
        }
        .menu-box li{
            width:33%;
            margin:0 1px 1px 0;
            padding:20px 0;
            float:left;
            background: #fff;
            font-size:16px;
            color:#333;
            text-align: center;
        }
        .menu-box li:last-child(3),
        .menu-box li:last-child{
            margin-right:0;
        }
        .menu-box li .menu-icon{
            display: block;
            width:38px;
            height:38px;
            margin:0 auto 5px auto;
        }
        #qrcode {
            width:160px;
            height:160px;
            margin-top:15px;
        }  


        /*数量框*/
        .box{
               
              
                
               
            }
            .box button{
                width: 30px;
                border: 1px solid #dedede;
            }
            .box input{
                width: 40px;
                text-align: center;
            }
            #money{
                border: none;
                text-align: left;
                margin-left: 2px;
            }
           
 
    </style>
</head>
<body>
@include('front.head')
<!-- <ul class="account-box clearfix">
    <li>账户余额：<span class="text-primary">0</span> 元</li>
    <li>认证状态：<span class="text-orange">未认证</span></li>
</ul> -->
@if($data)
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">选择充值套餐:</div>

  <!-- Table -->
  
    @foreach($data as $v)
    <table class="table">
    <tr>
        <td>套餐</td>
        <td>{{$v->name}}</td>
    </tr>
    <tr> 
        <td>价格</td>   
        <td>
            <span class="label label-danger">￥<span id="money">{{$v->price_txt}}</span>/月</span>
        </td>
    </tr>
    @if($v->id==1)
    <tr>
        <td>数量</td>    
        <td class="box">
           
            <button id="plus">+</button>
            <input type="text" id="num" value="3"/>
            <button id="subtract">-</button>个月
            
        
        </td>
    </tr>
    @endif
    <tr>    
        <td colspan="2" style="text-align: right">
            <button  type="button"  data-id="{{$v->id}}" data-price="{{$v->price}}" class="btn btn-primary pay-btn" data-id="{{$v->id}}">充值</button>
        </td>
    </tr>
    </table>
    <hr/>
    @endforeach
   
    
    
  
 <!--  <div style="float: right;margin-right:10px; ">
    <button id="pay" class="btn btn-primary navbar-btn">充值</a>
  </div> -->
</div>
<div id="qrcode"></div>
@else
<div class="alert alert-info" role="alert">没有查询到可以充值得套餐</div>
@endif
<script src="/front/bootstrap-3.3.6-dist/js/jquery.min.js"></script>
<script src="/front/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="http://res.wx.qq.com/open/js/jweixin-1.4.0.js"></script>
<script type="text/javascript" src="/js/jquery.qrcode.min.js"></script>
</body>
</html>
<script type="text/javascript">
    $('.pay-btn').click(function(){
        console.info('dianji');
        var data = {};
        var total_price  = 0;
        var num = 1;
        var id = $(this).data('id');
        if(id==1) {
             num  = $('#num').val();
        }
        
        var price        = $(this).data('price')
        total_price      = price*parseInt(num);
        data.dataplan_id = $(this).data('id');
        data.price  = total_price.toFixed(2);
        data.openid = "{{$user['openid']}}";
        data.iccid  = "{{$user['iccid']}}";
        data.num    = num;
        var url = "{{url('api/make-order')}}";
        $.post(url,data,function(rs){
            //alert(JSON.stringify(rs.data));
            if(rs.status==true) {
                wx.config({
                        debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                        appId: rs.data.appId, // 必填，公众号的唯一标识
                        timestamp: rs.data.timeStamp, // 必填，生成签名的时间戳
                        nonceStr: rs.data.nonceStr, // 必填，生成签名的随机串
                         signature: rs.data.signJs,
                        jsApiList: ['chooseWXPay'] // 必填，需要使用的JS接口列表，这里只写支付的
                    });

                wx.chooseWXPay({  
                    
                                    appId:     rs.data.appId,
                                    timestamp: rs.data.timeStamp,  
                    nonceStr: rs.data.nonceStr,  
                    package:  rs.data.package,  
                    signType: rs.data.signType,  
                    paySign:  rs.data.paySign, // 支付签名  
                    success: function (res) {  
                        // 支付成功后的回调函数  
                        window.location.href='/';  
                    },fail:function(res){
                                        alert(JSON.stringify(res));
                                    }, cancel: function(res){
                                            alert('取消支付');
                                    }  
                });  
            } else {
                alert(JSON.stringify(rs));
            }            
        });  
    });

  $('#subtract').on('click',function(){
    var num = $('#num').val();
    if(num<2) {
        alert('购买数量不能小于1');
        return ;
    }
    $('#num').val(parseInt(num)-1);
    var price = $('.pay-btn').data('price');
    $('#money').val(num*price/100);

  }); 

  $('#plus').on('click',function(){
    var num = $('#num').val();
    $('#num').val(parseInt(num)+1);
    var price = $('.pay-btn').data('price');
    $('#money').val(num*price/100);

  });  

  
   
            


</script>