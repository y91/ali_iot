<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width,height=device-height,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!--1.width=device-width    //应用程序的宽度和屏幕的宽度是一样的
2.height=device-height  //应用程序的高度和屏幕的高是一样的
3.initial-scale=1.0  //应用程序启动时候的缩放尺度（1.0表示不缩放）
4.minimum-scale=1.0  //用户可以缩放到的最小尺度（1.0表示不缩放）
5.maximum-scale=1.0  //用户可以放大到的最大尺度（1.0表示不缩放）
6.user-scalable=no  //用户是否可以通过他的手势来缩放整个应用程序，使应用程序的尺度发生一个改变（yes/no）
    -->
    <!-- 优先使用 IE 最新版本和 Chrome -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <!-- 启用360浏览器的极速模式(webkit) -->
    <meta name="renderer" content="webkit">
    <link href="https://cdn.bootcss.com/twitter-bootstrap/4.1.0/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="/front/iconfont/iconfont.css">
    <title>登录</title>
    <style>
        * {
            marign: 0;
            padding: 0;
            overflow: hidden;
        }

        html, body {
            width: 100%;
            height: 100%;
        }

        body {
            /*background:#0a6dbe;*/
            background: #0d73bd url("/front/bg.png") no-repeat;
            background-size: cover;
        }

        .login-icon {
            display: block;
            width: 130px;
            height: 130px;
            margin: 20px auto;
        }

        .login-box {
            width: 80%;
            margin: 0 auto;
            padding-top: 20%;
        }

        .input-box {
            margin: 10px 0;
            border-bottom: 1px solid rgba(255, 255, 255, 0.4);
        }

        .icon-input {
            display: inline-block;
            margin-right:5px;
            font-size: 16px;
            color: #fff;
            opacity: 0.6;
        }

        .noboder-input {
            height: 46px;
            line-height: 46px;
            background: transparent;
            border: none;
            outline: none;
            font-size: 16px;
            color: #fff;
        }

        .noboder-input::-webkit-input-placeholder { /* WebKit browsers */
            color: #fff;
        }

        .noboder-input:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
            color: #fff;
        }

        .noboder-input::-moz-placeholder { /* Mozilla Firefox 19+ */
            color: #fff;
        }

        .noboder-input:-ms-input-placeholder { /* Internet Explorer 10+ */
            color: #fff;
        }

        .noboder-input:focus::-webkit-input-placeholder { /* WebKit browsers */
            color: transparent;
        }

        .noboder-input:focus:-moz-placeholder { /* Mozilla Firefox 4 to 18 */
            color: transparent;
        }

        .noboder-input:focus::-moz-placeholder { /* Mozilla Firefox 19+ */
            color: transparent;
        }

        .noboder-input:focus:-ms-input-placeholder { /* Internet Explorer 10+ */
            color: transparent;
        }
        .button-box{
            width:90%;
            margin: 30px auto 0 auto;
        }
        .login-btn{
            width:100%;
            height:48px;
            font-size:20px;
            letter-spacing:10px;
            display: block;
            text-align: center;
            border:none;
            border-radius: 3px;
            background: rgba(255, 255, 255,0.4);
            color: rgba(255, 255, 255,0.8);
            font-weight:400;
        }
        .link-btn{
            margin-top:10px;
            display: block;
            float:right;
            font-size:12px;
            color:#afdef5;
        }
    </style>
</head>
<body>

<img src="/front/login-icon.png" alt="" class="login-icon">
<form action="{{url('logindo')}}" class="login-box">
@if(session('rs'))
<div class="alert alert-{{session('rs')['status']}}">
  {{ session('rs')['msg'] }}
</div>
@endif  
    <div class="input-box">
        <span class="icon-input iconfont icon-shoujihao1"></span>
        <input type="text" name="iccid" class="noboder-input" placeholder="请输入卡号">
    </div>
  <!--   <div class="input-box">
        <span class="icon-input iconfont icon-mima"></span>
        <input type="password" class="noboder-input" placeholder="请输入密码">
    </div> -->
    <div class="button-box clearfix">
        <button class="login-btn" type="button"> 登录</button>
        <!-- <a href="" class="link-btn">忘记密码</a> -->
    </div>
</form>
</body>
</html>
<script src="/front/bootstrap-3.3.6-dist/js/jquery.min.js"></script>
<script type="text/javascript">
    $(function(){
        var iccid  = localStorage.getItem("iccid");
        if(iccid) {
            $('input[name="iccid"]').val(iccid);    
        }
        
    });
    $('.login-btn').on('click',function(){
        var iccid = $('input[name="iccid"]').val();
        if(!iccid) {
            alert('请先输入卡号');return false;
        }
        localStorage.setItem('iccid', iccid);
        $('.login-box').submit();
    });
</script>