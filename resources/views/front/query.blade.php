<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width,height=device-height,initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!--1.width=device-width    //应用程序的宽度和屏幕的宽度是一样的
2.height=device-height  //应用程序的高度和屏幕的高是一样的
3.initial-scale=1.0  //应用程序启动时候的缩放尺度（1.0表示不缩放）
4.minimum-scale=1.0  //用户可以缩放到的最小尺度（1.0表示不缩放）
5.maximum-scale=1.0  //用户可以放大到的最大尺度（1.0表示不缩放）
6.user-scalable=no  //用户是否可以通过他的手势来缩放整个应用程序，使应用程序的尺度发生一个改变（yes/no）
    -->
    <!-- 优先使用 IE 最新版本和 Chrome -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <!-- 启用360浏览器的极速模式(webkit) -->
    <meta name="renderer" content="webkit">
    <link rel="stylesheet" href="/front/bootstrap-3.3.6-dist/css/bootstrap.min.css">

    <title>个人中心</title>
    <style>
        *{
            marign:0;
            padding:0
        }
        li{
            list-style: none;
        }
        body {
            background: #f0f1f3;
            font-size: 14px;
        }
        .text-orange{
            color:#FF7F42;
        }
        .head-box {
            padding: 20px 15px;
            background: #0d73bd url("/front/bg.png");
            background-size: cover;
        }

        .head-img-box {
            margin-right: 2%;
            float: left;
        }

        .head-img {
            width: 80px;
            height: 80px;
        }

        .member-name-box {
            width: 70%;
            margin: 12px 0 0 0;
            float: left;
            font-size: 14px;
            color: #fff;
        }
        .account-box{
            width:100%;
            height:46px;
            line-height: 46px;
            display:flex;
        }
        .account-box li{
            float:left;
            flex:1;
            background: #fff;
            text-align: center;
        }
        .account-box li:first-child{
            margin-right:2px;
        }
        .menu-box{
            margin-top:15px;
        }
        .menu-box li{
            width:33%;
            margin:0 1px 1px 0;
            padding:20px 0;
            float:left;
            background: #fff;
            font-size:16px;
            color:#333;
            text-align: center;
        }
        .menu-box li:last-child(3),
        .menu-box li:last-child{
            margin-right:0;
        }
        .menu-box li .menu-icon{
            display: block;
            width:38px;
            height:38px;
            margin:0 auto 5px auto;
        }
    </style>
</head>
<body>
@include('front.head')
<!-- <ul class="account-box clearfix">
    <li>账户余额：<span class="text-primary">0</span> 元</li>
    <li>认证状态：<span class="text-orange">未认证</span></li>
</ul> -->
@if($data)
<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">流量查询结果:</div>

  <!-- Table -->
  @foreach($data as $k=>$v)
  <table class="table">
    <tr>
        <td>总流量</td><td>{{$v['total']}}</td>
    </tr>
   
    <tr>
        <td>已用流量</td><td>{{$v['used']}}</td>
    </tr>
   
    <tr>
        <td>剩余流量</td><td>{{$v['reset']}}</td>
    </tr>
    
     <tr>
        <td>生效日期</td><td>{{$v['validDate']}}</td>
    </tr>
     <tr>
        <td>失效日期</td><td>{{$v['expireDate']}}</td>
    </tr>
    
  </table>
  @endforeach
  <div style="float: right;margin-right:10px; ">
    <a href="{{url('/')}}" class="btn btn-primary navbar-btn">返回</a>
  </div>
</div>
@else
<div class="alert alert-info" role="alert">没有查询到流量数据</div>
@endif
<script src="/front/bootstrap-3.3.6-dist/js/jquery.min.js"></script>
<script src="/front/bootstrap-3.3.6-dist/js/bootstrap.min.js"></script>
</body>
</html>