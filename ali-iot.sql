/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100132
 Source Host           : localhost:3306
 Source Schema         : ali-iot

 Target Server Type    : MySQL
 Target Server Version : 100132
 File Encoding         : 65001

 Date: 28/12/2018 09:02:16
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for admins
-- ----------------------------
DROP TABLE IF EXISTS `admins`;
CREATE TABLE `admins`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `salt` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '0',
  `role_id` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '管理员表' ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for cards
-- ----------------------------
DROP TABLE IF EXISTS `cards`;
CREATE TABLE `cards`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cardcode` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '卡号',
  `iccid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '终端号',
  `openid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `avatar` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of cards
-- ----------------------------
INSERT INTO `cards` VALUES (2, '1323320000323232', '232323', NULL, NULL, NULL, '2018-12-12 08:37:18', '2018-12-12 08:40:58');

-- ----------------------------
-- Table structure for dataplans
-- ----------------------------
DROP TABLE IF EXISTS `dataplans`;
CREATE TABLE `dataplans`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '套餐名',
  `offerid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '物联网阿里套餐',
  `price` decimal(10, 2) NULL DEFAULT NULL COMMENT '价格',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of dataplans
-- ----------------------------
INSERT INTO `dataplans` VALUES (1, '1G月包', '22010000250054', 10.00, '2018-12-12 11:26:02', '2018-12-12 11:26:02', NULL);

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` int(11) NULL DEFAULT NULL,
  `dataplan_id` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0' COMMENT '套餐id',
  `iccid` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '0',
  `price` decimal(10, 2) NULL DEFAULT NULL,
  `status` tinyint(1) NULL DEFAULT 0 COMMENT '0待付款 1已付款',
  `msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '备注信息',
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  INDEX `id`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES (1, 32223, '1', '32332', 10.00, 0, '232332', '2018-12-25 05:33:08', NULL);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `openid` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nickname` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信头像',
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `truename` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `mobile` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机号',
  `role_id` int(10) NULL DEFAULT NULL,
  `depart_id` int(10) NULL DEFAULT NULL,
  `status` int(10) NULL DEFAULT NULL COMMENT '0 冻结',
  `salt` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `api_token` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  `deleted_at` timestamp(0) NULL DEFAULT NULL,
  `source` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '', NULL, NULL, 'admin', 'efd9d1b8bfb00e8e3647990f7d74d1d8', NULL, '管理员', NULL, 100, NULL, NULL, '111111', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `users` VALUES (2, 'oYIp_1uCdkAJK9n5kgQGkJ1AdlxM', '毕兆尚', 'http://thirdwx.qlogo.cn/mmopen/nl74xdEzwWMpzyUiaQPM4kAJs3uFROn35x3Fiak9jT8TDbsCmibWpL8EIeNcleOl0OVfAfYCQibiaPxCzeyX1vxicOeSFOmiaAZx9Lc/132', '毕兆尚', 'da4286fc8443ebe9a015b29f2c5c8271', NULL, '哈哈', NULL, 100, NULL, NULL, 'JbPjiVBvXoPqrwbwmPTzMGqoNaL0UvmSVC9HIyIr', NULL, '2018-11-14 12:46:41', '2018-11-14 12:46:41', NULL, NULL);
INSERT INTO `users` VALUES (4, 'oYIp_1hivYYrO4iDwgiH4FGeJUcc', '蔡师傅精选', 'http://thirdwx.qlogo.cn/mmopen/VIhh6a29kBeVbQl6fE1WGsuGRk8o30c2znLgTxk1dzgH18e0UdrEAtnDKAJgPP5wtUCRmMsTiaRRySBnPvUKlOgKut5zCiaBu5/132', '蔡师傅精选', '21e806a053974eef05a303f26428a959', NULL, NULL, NULL, 1, NULL, NULL, 'SDTlnmexvsDAnOGicsoghEWp1MpMIbTM0eftNVJd', NULL, '2018-11-15 18:44:23', '2018-11-15 18:44:23', NULL, NULL);
INSERT INTO `users` VALUES (5, 'oYIp_1v5ML1527ge3VWM0BIz4nRw', '温科院蔡定岳（科苑信息）', 'http://thirdwx.qlogo.cn/mmopen/PiajxSqBRaEJy8aEwxCjJMtrSEnvxLy1PMTDspqS2xzM8cib86PnYhcppWPOaFKZLfC9IkYQficviaSOMkgAN8qPQA/132', '温科院蔡定岳（科苑信息）', '96f7d7f924498af1e861a14701dcefee', NULL, NULL, NULL, 100, NULL, NULL, 'u0XcftGvrJNClHQmrWW5gux5pEtSWxBlHrvesGfE', NULL, '2018-11-15 19:12:32', '2018-11-21 19:01:53', '2018-11-21 19:01:53', NULL);
INSERT INTO `users` VALUES (6, 'oYIp_1oHK6cPXTezOeRfwT-8gicg', 'anni', 'http://thirdwx.qlogo.cn/mmopen/ylRhrSjQb8huF8Vkbic9afmnypujn8PQbfmNPVIIdu3A9Ms2xYoEhiaa1Ap6PIvUSON7SEenav6ZP4j3vEI71ibCw/132', 'anni', 'de95a31c706553a6707cec6fbddd1ce0', NULL, '阿芬', NULL, 1, NULL, NULL, 'Pt8357q72a02qyRM8s2goacMjUBgs3RW9As40djI', NULL, '2018-11-17 19:41:42', '2018-11-17 19:41:57', NULL, NULL);
INSERT INTO `users` VALUES (8, 'oYIp_1vddqC0GkiNlxrlxqgCIqIk', '川哥', 'http://thirdwx.qlogo.cn/mmopen/nl74xdEzwWNZSeRfO1ODZwDorWyd5lu3c9CxSBzfIk17XH5uPwND6ESLibJq9Go8ZSEkRJU2ViaIdsm0ucdcXjYSWsO1OSmBNt/132', '川哥', '18878b8278958165183a8a4d84c690c2', 'racer.liu@gmail.com', '劉岩川', '15920790417', 100, 1, NULL, 'X9qzPAJXMoSxh9RASzSNL1ts3jwvK4vUNu2nkzas', NULL, '2018-11-25 18:33:10', '2018-11-25 18:37:31', '2018-11-25 18:37:31', '1234');
INSERT INTO `users` VALUES (9, 'oYIp_1vddqC0GkiNlxrlxqgCIqIk', '川哥', 'http://thirdwx.qlogo.cn/mmopen/nl74xdEzwWNZSeRfO1ODZwDorWyd5lu3c9CxSBzfIk17XH5uPwND6ESLibJq9Go8ZSEkRJU2ViaIdsm0ucdcXjYSWsO1OSmBNt/132', '川哥', '6250b834c7302b0106fa420b2ac9c966', NULL, NULL, NULL, 1, NULL, NULL, 'WUEkBciC8cnbz3tWxhm7U9gOktLClWXnabJaMwPk', NULL, '2018-11-25 18:36:59', '2018-11-25 18:36:59', NULL, NULL);
INSERT INTO `users` VALUES (10, 'oYIp_1jvUVK8W17phDmtbuZGUzYE', 'Racer', 'http://thirdwx.qlogo.cn/mmopen/H4C3LX20T4GNxoUK521kl2gUo8OIIc6e4Oib4sK7bHPspZsl1dkibYbAo1S4Rzh7zC0K61EBibVSMMyibLdGu46DogPclzs5k9yk/132', 'Racer', '3910adae98343438191ca8a950741081', NULL, NULL, NULL, 100, NULL, NULL, 'ZOo8LgO6p0vrWf428BjtnvsHMMSjqyVClgTAQocZ', NULL, '2018-11-25 18:38:07', '2018-11-25 18:38:07', NULL, NULL);
INSERT INTO `users` VALUES (11, 'oYIp_1rR0gF2NizT6TuKbJZgTxEY', '刘岩（友）川', 'http://thirdwx.qlogo.cn/mmopen/H4C3LX20T4GNxoUK521kl8umMvaAiaJtNMOUKRE6pSxamicyzHJTibrSYef7kHxzKzTibcc3UiaLOHrXRVU4doVK2icStVV0I4H32V/132', '刘岩（友）川', 'ba2722fd91053838b9dc66938a43854e', NULL, NULL, NULL, 500, NULL, NULL, '4xNtKT80sNS02fm7eqeA6LMgrcAIIRCrwpZkHrZm', NULL, '2018-11-25 19:13:17', '2018-11-25 19:13:17', NULL, NULL);
INSERT INTO `users` VALUES (12, 'oYIp_1i0eHZUS_3KNoZ3SNL6-mXc', '小何', 'http://thirdwx.qlogo.cn/mmopen/nl74xdEzwWMpzyUiaQPM4kIkJD5nGgCatJhEictFaiaeBmOktQxAmwvObkHxThnILpEg2X71zl6kG2gEKlK6ia2e4TwOFUib759fg/132', '小何', '7d1d1a997867fd9cd2975df2f64b79dd', NULL, NULL, NULL, 100, NULL, NULL, 'mT4aQXgTviUq6RwpqClKH6L3xEyhd9F5bhHjIyeF', NULL, '2018-11-26 20:58:34', '2018-11-26 20:58:34', NULL, NULL);
INSERT INTO `users` VALUES (13, NULL, NULL, NULL, '13777820592', '13535763905516fa1f2d13bb6ef47b71', '345', 'dd', '13777820592', 1, NULL, NULL, 'blBN8rTc0DEBIjIiJMgRmgj8E3oMLq2d3YlGTdne', NULL, '2018-12-05 11:13:13', '2018-12-05 11:13:59', '2018-12-05 11:13:59', 'dd');

SET FOREIGN_KEY_CHECKS = 1;
